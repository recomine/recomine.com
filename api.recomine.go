package main

import (
	"database/sql"
	"encoding/json"
	"github.com/go-martini/martini"
	"github.com/joho/godotenv"
	"github.com/martini-contrib/render"
	"github.com/martini-contrib/sessionauth"
	"github.com/martini-contrib/sessions"
	"github.com/mattbaird/gochimp"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
)

func ApiPostUpload(renderTo render.Render, request *http.Request) {
	log.Println(request.FormValue("businessShortName"))

	file, fileHeader, err := request.FormFile("files[]")

	if err != nil {
		log.Println("Error: can not retrieve file")
	} else {
		defer file.Close()
		log.Println(fileHeader.Filename)
		log.Println(fileHeader.Header)
		aNewImage := newUserImage(5685, 0)
		err = dbmap.Insert(&aNewImage)
		if err == nil {
			imagePath, err := UploadImageOfSizes(file, 5685, aNewImage.Id, picoSize, nanoSize)
			if err != nil {
				log.Println("Error: can not upload image")
			} else {
				//existingRecominee.ProfileImageUrl = imagePath
				aNewImage.Url = imagePath
				_, err = dbmap.Update(&aNewImage)
				if err != nil {
					log.Println("Error: can not update image URL")
				}

				images := make([]UserImage, 1)
				renderTo.JSON(http.StatusOK, map[string]interface{}{"files": images})
			}
		}
	}
}

func ApiGetFeedback(renderTo render.Render, user sessionauth.User) {
	theRecominee := Recominee{}

	var feedbacks []Feedback

	err := theRecominee.GetByMasterId(user.UniqueId())

	if err == nil {
		_, err := dbmap.Select(&feedbacks, "SELECT * FROM feedback WHERE owner_id=$1 AND in_reply_to = 0 ORDER BY created DESC", theRecominee.Id)
		if err != nil {
			renderTo.JSON(http.StatusFound, map[string]string{"status": err.Error()})
			return
		}
	} else if err == sql.ErrNoRows {
		renderTo.JSON(http.StatusFound, map[string]string{"status": "not found"})
		return
	} else {
		renderTo.JSON(http.StatusFound, map[string]string{"status": err.Error()})
		return
	}

	for _, aFeedback := range feedbacks {
		aFeedback.Uploader = fetchRecominerDetailFromId(aFeedback.UploaderId)
		aFeedback.Owner = fetchRecomineeDetailFromId(aFeedback.OwnerId)
		aFeedback.FeedbackImage = fetchImageFromFeedbackId(aFeedback.Id)
		aFeedback.Replies = fetchFeedbackRepliesFromFeedbackId(aFeedback.Id)
	}
	renderTo.JSON(http.StatusOK, feedbacks)
}

func ApiGetRecominerFeedback(renderTo render.Render, user sessionauth.User) {
	var feedbacks []Feedback

	_, err := dbmap.Select(&feedbacks, "SELECT * FROM feedback WHERE uploader_id=$1 AND in_reply_to = 0 ORDER BY created DESC", user.UniqueId())
	if err != nil {
		renderTo.JSON(http.StatusFound, map[string]string{"status": err.Error()})
		return
	}

	for _, aFeedback := range feedbacks {
		aFeedback.Uploader = fetchRecominerDetailFromId(aFeedback.UploaderId)
		aFeedback.Owner = fetchRecomineeDetailFromId(aFeedback.OwnerId)
		aFeedback.FeedbackImage = fetchImageFromFeedbackId(aFeedback.Id)
		aFeedback.Replies = fetchFeedbackRepliesFromFeedbackId(aFeedback.Id)
	}
	renderTo.JSON(http.StatusOK, feedbacks)
}

func ApiGetRecomineeFeedback(renderTo render.Render, user sessionauth.User) {
	theRecominee := Recominee{}

	var feedbacks []Feedback

	err := theRecominee.GetByMasterId(user.UniqueId())

	if err == nil {
		_, err := dbmap.Select(&feedbacks, "SELECT * FROM feedback WHERE owner_id=$1 AND in_reply_to = 0 ORDER BY created DESC", theRecominee.Id)
		if err != nil {
			renderTo.JSON(http.StatusFound, map[string]string{"status": err.Error()})
			return
		}
	} else if err == sql.ErrNoRows {
		renderTo.JSON(http.StatusFound, map[string]string{"status": "not found"})
		return
	} else {
		renderTo.JSON(http.StatusFound, map[string]string{"status": err.Error()})
		return
	}

	for _, aFeedback := range feedbacks {
		aFeedback.Uploader = fetchRecominerDetailFromId(aFeedback.UploaderId)
		aFeedback.Owner = fetchRecomineeDetailFromId(aFeedback.OwnerId)
		aFeedback.FeedbackImage = fetchImageFromFeedbackId(aFeedback.Id)
		aFeedback.Replies = fetchFeedbackRepliesFromFeedbackId(aFeedback.Id)
	}
	renderTo.JSON(http.StatusOK, feedbacks)
}

func ApiPostFeedback(renderTo render.Render, authUser sessionauth.User, request *http.Request, params martini.Params) {
	activeRecominer := Recominer{}
	err := activeRecominer.GetById(authUser.UniqueId())
	if err != nil {
		log.Print(err.Error())
	}

	viewedRecominee := Recominee{}
	err = viewedRecominee.GetByUserName(params["recominee"])
	if err != nil {
		log.Print(err.Error())
	}

	aNewFeedback := newFeedback(request.FormValue("message"), activeRecominer.Id, viewedRecominee.Id, 0)

	if request.FormValue("complaint") == "on" {
		aNewFeedback.Category = "complaint"
	} else if request.FormValue("compliment") == "on" {
		aNewFeedback.Category = "compliment"
	} else if request.FormValue("question") == "on" {
		aNewFeedback.Category = "question"
	} else if len(request.FormValue("categorySelect")) > 0 {
		aNewFeedback.Category = request.FormValue("categorySelect")
	} else {
		aNewFeedback.Category = "suggestion"
	}

	err = dbmap.Insert(&aNewFeedback)
	if err != nil {
		log.Print(err.Error())
	}

	file, fileHeader, err := request.FormFile("files[]")

	if err != nil {
		log.Println("Error: can not retrieve file")
		renderTo.HTML(http.StatusOK, "recominee", map[string]interface{}{"activeRecominer": activeRecominer, "viewedRecominee": viewedRecominee, "alert": "Posting feedback success!"})
	} else {
		defer file.Close()
		log.Println(fileHeader.Filename)
		// log.Println(fileHeader.Header)
		aNewImage := newUserImage(activeRecominer.Id, aNewFeedback.Id)
		err = dbmap.Insert(&aNewImage)

		if err == nil {
			imagePath, err := UploadImageOfSizes(file, activeRecominer.Id, aNewImage.Id, tinySize, mediumSize)
			if err != nil {
				log.Println("Error: can not upload image")
			} else {
				aNewImage.Url = imagePath
				_, err = dbmap.Update(&aNewImage)
				if err != nil {
					log.Println("Error: can not update image URL")
				}

				imageMap := map[string]interface{}{"name": filepath.Base(imagePath), "size": nanoSize.Size, "url": "http://media.recomine.com/m/" + imagePath, "thumbnailUrl": "http://media.recomine.com/t/" + imagePath}

				images := make([]map[string]interface{}, 1)
				images[0] = imageMap

				aNewFeedback.ImageId = aNewImage.Id
				_, err = dbmap.Update(&aNewFeedback)
				if err != nil {
					log.Println("Error: can not update feedback image Url")
				}

				renderTo.JSON(http.StatusOK, map[string]interface{}{"files": images})
			}
		}
	}
}

func ApiGetFeedbackById(renderTo render.Render, authUser sessionauth.User, params martini.Params) {
	theFeedback := Feedback{}

	err := dbmap.SelectOne(&theFeedback, "SELECT * FROM feedback WHERE feedback_id = $1", params["feedbackId"])

	if err == sql.ErrNoRows {
		renderTo.JSON(http.StatusFound, map[string]string{"status": "error not found"})
		return
	} else if err != nil {
		renderTo.JSON(http.StatusFound, map[string]string{"status": "error"})
		checkErr(err, "error getting feedback by id")
		return
	}

	renderTo.JSON(http.StatusOK, theFeedback)
}

func ApiGetFeedbackReplies(renderTo render.Render, authUser sessionauth.User, params martini.Params) {
	var feedbacks []Feedback

	_, err := dbmap.Select(&feedbacks, "SELECT * FROM feedback WHERE in_reply_to = $1", params["feedbackId"])

	if err == sql.ErrNoRows {
		renderTo.JSON(http.StatusFound, map[string]string{"status": "error not found"})
		return
	} else if err != nil {
		renderTo.JSON(http.StatusFound, map[string]string{"status": "error"})
		return
	}

	renderTo.JSON(http.StatusOK, feedbacks)
}

func ApiPostFeedbackReply(renderTo render.Render, request *http.Request, session sessions.Session, authUser sessionauth.User, params martini.Params) {
	aFeedback := Feedback{}
	body, err := ioutil.ReadAll(request.Body)
	checkErr(err, "error reading body request")
	err = json.Unmarshal(body, &aFeedback)
	checkErr(err, "error unmarshal json")

	activeRecominer := Recominer{}
	err = activeRecominer.GetById(authUser.UniqueId())
	checkErr(err, "error getting recominer detail")

	feedbackID, err := strconv.ParseInt(params["feedbackId"], 10, 64)
	checkErr(err, "error converting to integer")

	parentFeedback := Feedback{}
	err = parentFeedback.GetById(feedbackID)
	checkErr(err, "error getting parent feedback")

	aNewFeedback := newFeedback(aFeedback.Message, activeRecominer.Id, 4, feedbackID)
	aNewFeedback.Category = "reply"

	err = dbmap.Insert(&aNewFeedback)

	if err != nil {
		checkErr(err, "error inserting new feedback")
		renderTo.JSON(http.StatusFound, map[string]interface{}{"status": "error"})
	} else {
		aNewFeedback.Uploader = fetchRecominerDetailFromId(aNewFeedback.UploaderId)
		aNewFeedback.Owner = fetchRecomineeDetailFromId(aNewFeedback.OwnerId)

		renderTo.JSON(http.StatusOK, map[string]interface{}{"status": "success", "data": aNewFeedback})

		err = godotenv.Load()
		if err != nil {
			log.Println("Error: error loading environment")
		}

		apiKey := os.Getenv("MANDRILL_KEY")
		mandrillApi, err := gochimp.NewMandrill(apiKey)

		if err != nil {
			log.Println("Error instantiating client")
		}

		templateName := "feedback-notification"
		contentVar := gochimp.Var{"main", "<h1>Welcome aboard!</h1>"}
		content := []gochimp.Var{contentVar}

		renderedTemplate, err := mandrillApi.TemplateRender(templateName, content, nil)

		if err != nil {
			log.Println("Error rendering template")
		}

		recipients := []gochimp.Recipient{
			gochimp.Recipient{Email: activeRecominer.Email},
			gochimp.Recipient{Email: GetUploaderEmailOfFeedbackId(aNewFeedback.InReplyTo)},
		}

		message := gochimp.Message{
			Html:      renderedTemplate,
			Subject:   "New feedback reply waiting!",
			FromEmail: "halo@recomine.com",
			FromName:  "Recomine",
			To:        recipients,
		}

		_, err = mandrillApi.MessageSend(message, false)

		if err != nil {
			log.Println("Error sending message")
		}
	}
}

func ApiGetCheckRecominer(renderTo render.Render, request *http.Request) {
	if (len(request.FormValue("initialName")) > 0) && (request.FormValue("initialName") == request.FormValue("name")) {
		renderTo.JSON(http.StatusOK, map[string]interface{}{"valid": true})
	} else {
		recominerDuplicate := IsUserNameDuplicate(request.FormValue("name"))
		renderTo.JSON(http.StatusOK, map[string]interface{}{"valid": !recominerDuplicate})
	}
}

func ApiGetEmailCheckRecominer(renderTo render.Render, request *http.Request) {
	if (len(request.FormValue("initialEmail")) > 0) && (request.FormValue("initialEmail") == request.FormValue("email")) {
		renderTo.JSON(http.StatusOK, map[string]interface{}{"valid": true})
	} else {
		recominerEmailDuplicate := IsEmailDuplicate(request.FormValue("email"))
		renderTo.JSON(http.StatusOK, map[string]interface{}{"valid": !recominerEmailDuplicate})
	}
}

func ApiGetCheckRecominee(renderTo render.Render, request *http.Request) {
	if (len(request.FormValue("initialName")) > 0) && (request.FormValue("initialName") == request.FormValue("businessShortName")) {
		renderTo.JSON(http.StatusOK, map[string]interface{}{"valid": true})
	} else {
		recomineeDuplicate := IsBusinessShortNameDuplicate(0, request.FormValue("businessShortName"))
		renderTo.JSON(http.StatusOK, map[string]interface{}{"valid": !recomineeDuplicate})
	}
}
