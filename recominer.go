package main

import (
	"database/sql"
	"github.com/martini-contrib/sessionauth"
	"log"
	"time"
)

// Recominer can be any struct that represents a user in my system
type Recominer struct {
	Id              int64     `form:"-" db:"id" json:"id"`
	Name            string    `form:"name" db:"name" json:"name"`
	Email           string    `form:"email" db:"email" json:"email"`
	Password        string    `form:"password" db:"password" json:"-"`
	authenticated   bool      `form:"-" db:"-" json:"-"`
	Created         int64     `form:"-" db:"created" json:"created"`
	Updated         int64     `form:"-" db:"updated" json:"-"`
	LastLogin       int64     `form:"-" db:"last_login" json:"-"`
	FullName        string    `form:"fullname" db:"full_name" json:"full_name"`
	Phone           string    `form:"phone" db:"phone" json:"-"`
	Address         string    `form:"address" db:"address" json:"-"`
	Sex             string    `form:"sex" db:"sex" json:"-"`
	Birthplace      string    `form:"birthplace" db:"birthplace" json:"-"`
	Birthday        string    `form:"birthday" db:"birthday" json:"-"`
	ProfileImageUrl string    `form:"-" db:"profile_image_url" json:"profile_image_url"`
	HeaderImageUrl  string    `form:"-" db:"header_image_url" json:"header_image_url"`
	Bio             string    `form:"bio" db:"bio" json:"bio"`
	Facebook        string    `form:"facebook" db:"facebook" json:"-"`
	Twitter         string    `form:"twitter" db:"twitter" json:"-"`
	OwnedRecominee  Recominee `form:"-" db:"-" json:"owned_recominee"`
	EmailVerified   bool      `form:"-" db:"email_verified"`
	// Reference     string `form:"-" db:"reference"`
	// Interest      string `form:"-" db:"interest"`
}

// GetAnonymousUser should generate an anonymous user model
// for all sessions. This should be an unauthenticated 0 value struct.
func GenerateAnonymousUser() sessionauth.User {
	return &Recominer{}
}

func newRecominer(email string, password string, name string, authenticated bool) Recominer {
	return Recominer{
		Email:         email,
		Password:      password,
		Name:          name,
		Created:       time.Now().Unix(),
		Updated:       time.Now().Unix(),
		EmailVerified: false,
		authenticated: authenticated,
	}
}

// Login will preform any actions that are required to make a user model
// officially authenticated.
func (recominer *Recominer) Login() {
	// Update last login time
	// Add to logged-in user's list
	// etc ...
	recominer.authenticated = true
	recominer.LastLogin = time.Now().Unix()
}

// Logout will perform any actions that are required to completely
// logout a user.
func (recominer *Recominer) Logout() {
	// Remove from logged-in user's list
	// etc ...
	recominer.authenticated = false
}

func (recominer *Recominer) IsAuthenticated() bool {
	return recominer.authenticated
}

func (recominer *Recominer) UniqueId() interface{} {
	return recominer.Id
}

// GetById will populate a user object from a database model with
// a matching id.
func (recominer *Recominer) GetById(id interface{}) error {
	err := dbmap.SelectOne(recominer, "SELECT * FROM recominer WHERE id = $1", id)
	if err != nil {
		return err
	}

	return nil
}

func (recominer *Recominer) GetByUserName(username interface{}) error {
	err := dbmap.SelectOne(recominer, "SELECT * FROM recominer WHERE name = $1", username)
	if err != nil {
		return err
	}

	return nil
}

func IsUserNameDuplicate(username string) bool {
	recominer := Recominer{}
	err := dbmap.SelectOne(&recominer, "SELECT * FROM recominer WHERE name = $1", username)
	if err == sql.ErrNoRows {
		return false
	} else if err != nil {
		checkErr(err, "Error fetching user with username")
		return false
	}

	return true
}

func IsEmailDuplicate(email interface{}) bool {
	recominer := Recominer{}
	err := dbmap.SelectOne(&recominer, "SELECT * FROM recominer WHERE email = $1", email)
	if err == sql.ErrNoRows {
		return false
	} else if err != nil {
		log.Println("Error fetching recominer with email, ", err.Error())
		return true
	}
	return true
}

func fetchRecominerDetailFromId(id interface{}) (aRecominer Recominer) {
	err := dbmap.SelectOne(&aRecominer, "SELECT * FROM recominer WHERE id = $1", id)
	if err != nil {
		log.Println("Error fetching recominer detail, ", err.Error())
	}

	aRecominer.OwnedRecominee = fetchRecomineeDetailFromMasterId(aRecominer.Id)

	return
}
