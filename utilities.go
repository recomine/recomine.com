package main

import (
	"code.google.com/p/go.crypto/bcrypt"
	"fmt"
	"log"
	"time"
)

// A function for adding two numbers. It is needed on the html template.
// @params: m and n, each an integer.
// @return: a string of of sum of the parameters

func lenPlus(m int, n int) string {
	return fmt.Sprintf("%d", m+n)
}

// A function for formatting the raw unix time into proper date and time. It is needed on the html template.
// @params: an integer of unix time
// @return: a time string formatted as dd MM hh:mm:ss

func formatTime(args ...interface{}) string {
	t1 := time.Unix(args[0].(int64), 0)
	return t1.Format(time.Stamp)
}

// A function to count category occurence from a feedbacks slice. It is needed on the html template.
// @params: category name (string); the slice of feedbacks ([]Feedback)
// @return: an integer of occurence of the category name on the slices. If none, then it will return zero.

func countCategoryFromSlices(categoryName string, feedbacks []Feedback) int {
	counter := 0
	for _, feedback := range feedbacks {
		if feedback.Category == categoryName {
			counter++
		}
	}
	return counter
}

// A generic function to check for error and print a custom message on log.
// @params: an error type (error); a string of message (string)
// @return: none. Prints the error and message to log.

func checkErr(err error, msg string) {
	if err != nil {
		log.Fatalln(msg, err)
	}
}

func printErr(err error, msg string) {
	if err != nil {
		log.Println(msg, err)
	}
}

// A function to check whether a slice of recominees contains a particular username
// @params: a slice of recominees ([]Recominee); the username to be checked (string)
// @return: boolean. true if the slice contains the particular username, false if not.

func recomineesContainsUserName(recominees []Recominee, username string) bool {
	for _, aRecominee := range recominees {
		if aRecominee.ShortName == username {
			return true
		}
	}
	return false
}

// A function to check the password against the database
// @params: password to be checked (string); user ID (interface{})
// @return: true if password match, false if password not match.

func isPasswordCorrect(password string, userID interface{}) bool {
	thePassword, err := dbmap.SelectStr("SELECT password FROM recominer WHERE id=$1", userID)
	checkErr(err, "error retrieving password")
	return bcrypt.CompareHashAndPassword([]byte(thePassword), []byte(password)) != nil
}
