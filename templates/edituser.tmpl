<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Recomine - Customer Feedback Management</title>

    <link href="/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="/css/recomine.css" rel="stylesheet"/>

    <!-- start Mixpanel --><script type="text/javascript">(function(f,b){if(!b.__SV){var a,e,i,g;window.mixpanel=b;b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==typeof d?c=b[d]=[]:d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};i="disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.track_charge people.clear_charges people.delete_user".split(" ");
        for(g=0;g<i.length;g++)f(c,i[g]);b._i.push([a,e,d])};b.__SV=1.2;a=f.createElement("script");a.type="text/javascript";a.async=!0;a.src="//cdn.mxpnl.com/libs/mixpanel-2.2.min.js";e=f.getElementsByTagName("script")[0];e.parentNode.insertBefore(a,e)}})(document,window.mixpanel||[]);
        mixpanel.init("3185f6f34dfcaccf49aff7d2dc87439e");</script><!-- end Mixpanel -->
</head>
<body>
    <script type="text/javascript">mixpanel.track("Edit User Load");</script>
    <nav class="navbar navbar-default" role="navigation">
        <div class="container">
            <div class="navbar-header">
              <button type="button" class="btn btn-link navbar-toggle collapsed" id="profileDropdownMenuSmall" data-toggle="collapse" data-target="#collapseNavbar">
                <img class="img-rounded" src="[[if .activeRecominer.ProfileImageUrl]]http://media.recomine.com/p/[[.activeRecominer.ProfileImageUrl]][[else]]/images/personal20.png[[end]]" alt=""> <span class="caret"></span>
              </button>
              <a class="navbar-brand" href="/"><img src="/images/logoLightExtraSmall.png" alt="Recomine Logo"></a>
            </div>
            <div class="hidden-xs" id="mainNavbar">
              <ul class="nav navbar-nav navbar-right">
                  <li class="dropdown">
                      <button class="btn navbar-btn dropdown-toggle" id="profileDropdownMenu" data-toggle="dropdown"><img class="img-rounded" src="[[if .activeRecominer.ProfileImageUrl]]http://media.recomine.com/p/[[.activeRecominer.ProfileImageUrl]][[else]]/images/personal20.png[[end]]" alt="">[[if .activeRecominer.FullName]] [[.activeRecominer.FullName]] [[else]] [[.activeRecominer.Name]] [[end]] <span class="caret"></span></button>
                      <ul class="dropdown-menu" role="menu" aria-labelledby="profileDropdownMenu">
                          <li role="presentation"><a role="menuitem" tabindex="1" href="/dashboard">Dashboard</a></li>
                          <li role="presentation" class="divider"></li>
                          <li role="presentation"><a role="menuitem" tabindex="2" href="/us/recominee">Edit Business</a></li>
                          <li role="presentation"><a role="menuitem" tabindex="3" href="/me/[[.activeRecominer.Name]]/edit">Edit Profile</a></li>
                          <li role="presentation"><a role="menuitem" tabindex="4" href="/me/[[.activeRecominer.Name]]/password">Edit Password</a></li>
                          <li role="presentation" class="divider"></li>
                          <li role="presentation"><a role="menuitem" tabindex="5" href="/logout">Log Out</a></li>
                      </ul>
                  </li>
              </ul>
            </div>
            <div class="collapse navbar-collapse hidden-sm hidden-md hidden-lg" id="collapseNavbar">
              <ul class="nav navbar-nav hidden-sm hidden-md hidden-lg">
                  <li class="hidden-md hidden-lg"><a href="/dashboard">Dashboard</a></li>
                  <li class="hidden-md hidden-lg"><a href="/us/recominee">Edit Business</a></li>
                  <li class="hidden-md hidden-lg"><a href="/me/[[.activeRecominer.Name]]/edit">Edit Profile</a></li>
                  <li class="hidden-md hidden-lg"><a href="/me/[[.activeRecominer.Name]]/password">Edit Password</a></li>
                  <li class="hidden-md hidden-lg"><a href="/logout">Log Out</a></li>
              </ul>
            </div><!-- /.navbar-collapse -->
        </div>
    </nav>

    <div class="editContent">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">
                    [[if .alert]]
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-[[.alertType]] alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert">
                                <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                [[.alert]] 
                                [[if .firstTime]]<a type="button" class="btn btn-primary pull-right" href="/us/recominee"><span class="glyphicon glyphicon-plus"></span> Add business</a><br>Have you added your business?
                                [[end]]
                            </div>
                        </div>
                    </div>
                    [[end]]
                    <form id="editRecominerForm" action="/me/[[.activeRecominer.Name]]/edit" method="POST" enctype="multipart/form-data">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h1 class="panel-title">Edit User Profile</h1>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-2">
                                        <img class="img-rounded img-responsive" src="[[if .activeRecominer.ProfileImageUrl]]http://media.recomine.com/t/[[.activeRecominer.ProfileImageUrl]][[else]]/images/personal80.png[[end]]" alt="">
                                    </div>
                                    <div class="col-md-10">
                                        <div class="form-group">
                                            <label for="inputProfilePicture">Profile Picture</label>
                                            <input type="file" id="inputProfilePicture" name="inputPicture">
                                            <p class="help-block">Upload your jpg/png/gif file with maximum size of 2MB. We recommended a square image size for best presentation.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" >
                                            <label for="inputName">User Name <small>(required)</small></label>
                                            <div class="input-group">
                                                <span class="input-group-addon">http://recomine.com/me/</span>
                                                <input type="text" name="name" class="form-control" id="inputName" placeholder="This will be your personal URL. No spaces or symbol allowed." value="[[.activeRecominer.Name]]">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail">Email <small>(required)</small></label>
                                            <input type="text" name="email" class="form-control" id="inputEmail" placeholder="What is your email address?" value="[[.activeRecominer.Email]]">
                                        </div>
                                        <div class="form-group">
                                            <label for="inputFullName">Full Name</label>
                                            <input type="text" name="fullname" class="form-control" id="inputFullName" placeholder="What is your full name" value="[[.activeRecominer.FullName]]">
                                        </div>
                                        <div class="form-group">
                                            <label for="inputSex">Sex</label>
                                            <select name="sex" class="form-control" id="inputSex">
                                                <option [[if eq .activeRecominer.Sex "Male"]]selected="selected"[[end]]>Male</option>
                                                <option [[if eq .activeRecominer.Sex "Female"]]selected="selected"[[end]]>Female</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputBio">Short Bio</label>
                                            <textarea name="bio" class="form-control" rows="3" id="inputBio" placeholder="Describe yourself in less than 150 characters.">[[.activeRecominer.Bio]]</textarea>
                                            <small><span id="bioCount"></span> characters remaining</small>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputUserAddress">Address</label>
                                            <input type="text" name="address" class="form-control" id="inputUserAddress" placeholder="Where do you live?" value="[[.activeRecominer.Address]]">
                                        </div>
                                        <div class="form-group">
                                            <label for="inputPhoneNumber">Phone Number</label>
                                            <input type="text" name="phone" class="form-control" id="inputPhoneNumber" placeholder="How can people call you?" value="[[.activeRecominer.Phone]]">
                                        </div>
                                        <div class="form-group">
                                            <label for="inputFacebook">Facebook Account</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">facebook.com/</span>
                                                <input type="text" name="facebook" class="form-control" id="inputFacebook" placeholder="Do you have a facebook account?" value="[[.activeRecominer.Facebook]]">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputTwitter">Twitter Account</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">@</span>
                                                <input type="text" name="twitter" class="form-control" id="inputTwitter" placeholder="Or are you a twitter fan?" value="[[.activeRecominer.Twitter]]">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button id="resetButton" type="reset" class="btn btn-warning btn-lg"><span class="glyphicon glyphicon-repeat"></span> Reset </button>
                                <button type="submit" class="btn btn-primary btn-lg pull-right"><span class="glyphicon glyphicon-floppy-disk"></span> Save </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    <!-- <div class="footer">
        <nav class="navbar navbar-default navbar-fixed-bottom" role="navigation">
            <div class="container">
                <ul class="nav navbar-nav navbar-left hidden-xs">
                    <li><a href="http://blog.recomine.com" target="_blank">Blog</a>
                    </li>
                    <li><a href="https://recomine.com/us/recomine" target="_blank">Contact</a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com/recominer" target="_blank">
                            <span class="fa fa-facebook"></span>
                        </a>
                    </li>
                    <li>
                        <a href="http://twitter.com/recominer" target="_blank">
                            <span class="fa fa-twitter"></span>
                        </a>
                    </li>
                </ul>

                <p class="navbar-text navbar-right text-center">&#0169; 2014 Recomine.
                    All rights reserved.
                </p>
            </div>
        </nav>
    </div> -->

    <script src="/js/jquery-1.11.1.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/bootstrapValidator.min.js"></script>
    <script src="/js/edit_recominer.js"></script>
</body>
</html>