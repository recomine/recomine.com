package main

import (
	"github.com/nfnt/resize"
	"image"
	"image/gif"
	"image/jpeg"
	"image/png"
	"io"
	"log"
	"math/rand"
	"mime/multipart"
	"strconv"
	"time"
)

type UserImage struct {
	Id           int64     `db:"image_id" json:"id"`
	UploaderId   int64     `db:"uploader_id" json:"-"`
	Uploader     Recominer `db:"-" json:"uploader"`
	FeedbackId   int64     `db:"feedback_id" json:"-"`
	Url          string    `db:"url" json:"url"`
	FullUrl      string    `db:"-" json:"full_url"`
	ThumbnailUrl string    `db:"-" json:"thumbnail_url"`
	Created      int64     `db:"created" json:"created"`
	Updated      int64     `db:"updated" json:"-"`
}

type ImageSize struct {
	Size uint
	Code string
}

var picoSize = ImageSize{Size: 20, Code: "p"}
var nanoSize = ImageSize{Size: 40, Code: "n"}
var tinySize = ImageSize{Size: 80, Code: "t"}
var smallSize = ImageSize{Size: 160, Code: "s"}
var mediumSize = ImageSize{Size: 320, Code: "m"}
var largeSize = ImageSize{Size: 640, Code: "l"}
var hugeSize = ImageSize{Size: 1280, Code: "h"}

func newUserImage(uploaderId int64, feedbackId int64) UserImage {
	return UserImage{
		UploaderId: uploaderId,
		FeedbackId: feedbackId,
		Created:    time.Now().Unix(),
		Updated:    time.Now().Unix(),
	}
}

func UploadImageOfSizes(file multipart.File, uploaderId int64, mediaId int64, sizes ...ImageSize) (string, error) {

	var imagePath string

	randomFactor := rand.New(rand.NewSource(time.Now().UnixNano()))
	randomNumber := randomFactor.Int63()

	img, format, decodeErr := image.Decode(file)
	if decodeErr != nil {
		log.Println("Error: can not decode file")
		return imagePath, decodeErr
	}

	imagePath = getFilePath(randomNumber, mediaId, uploaderId, format)

	for _, value := range sizes {
		resizedImage := resize.Thumbnail(value.Size, value.Size, img, resize.Bicubic)
		urlPath, copyErr := PutImageToS3(resizedImage, value.Code, imagePath)
		if copyErr != nil {
			log.Println("Error: can not copy image to S3")
			return imagePath, copyErr
		}
		log.Println("Created image at: " + urlPath)
	}

	return imagePath, nil
}

func UploadImage(file multipart.File, uploaderId int64, mediaId int64) (string, error) {

	var imagePath string

	sizeVariants := [6]ImageSize{picoSize, nanoSize, tinySize, smallSize, mediumSize, largeSize}
	randomFactor := rand.New(rand.NewSource(time.Now().UnixNano()))
	randomNumber := randomFactor.Int63()

	img, format, decodeErr := image.Decode(file)
	if decodeErr != nil {
		log.Println("Error: can not decode file")
		return imagePath, decodeErr
	}

	imagePath = getFilePath(randomNumber, mediaId, uploaderId, format)

	for _, value := range sizeVariants {
		resizedImage := resize.Thumbnail(value.Size, value.Size, img, resize.Bicubic)
		filePath := value.Code + "-" + imagePath
		urlPath, copyErr := CopyImageToS3(resizedImage, filePath)
		if copyErr != nil {
			log.Println("Error: can not copy image to S3")
			return imagePath, copyErr
		}
		log.Println("Created image at: " + urlPath)
	}

	return imagePath, nil
}

// Image file name : size-randomNumber-MediaId-UploaderId.format

func getFilePath(randomNumber int64, mediaId int64, uploaderId int64, format string) string {
	return strconv.FormatInt(randomNumber, 10) + "_" + strconv.FormatInt(mediaId, 10) + "_" + strconv.FormatInt(uploaderId, 10) + "." + format
}

func EncodeImage(theImage image.Image, fileType string, writer io.Writer) (err error) {
	if fileType == ".jpeg" {
		err = jpeg.Encode(writer, theImage, nil)
	} else if fileType == ".png" {
		err = png.Encode(writer, theImage)
	} else if fileType == ".gif" {
		err = gif.Encode(writer, theImage, nil)
	}
	return
}

func fetchImageFromFeedbackId(feedbackid interface{}) (anImage UserImage) {
	err := dbmap.SelectOne(&anImage, "SELECT image_id, feedback_id, url FROM userimage WHERE feedback_id = $1", feedbackid)
	if err != nil {
		log.Println("Error fetching feedback image, ", err.Error())
	}

	return
}
