package main

import (
	"bytes"
	"github.com/joho/godotenv"
	"github.com/kr/s3"
	"image"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"
)

const (
	storageBaseUrl = "http://media.recomine.com"
)

func CopyImageToS3(theImage image.Image, filename string) (string, error) {
	var imagePath string

	envErr := godotenv.Load()
	if envErr != nil {
		log.Println("Error: can not set S3 environment")
		log.Println(os.Stderr, envErr)
		return imagePath, envErr
	}

	keys := s3.Keys{
		AccessKey: os.Getenv("S3_ACCESS_KEY"),
		SecretKey: os.Getenv("S3_SECRET_KEY"),
	}

	data := new(bytes.Buffer)
	encodeErr := EncodeImage(theImage, filepath.Ext(filename), data)
	if encodeErr != nil {
		log.Println("Error: can not encode image")
		log.Println(os.Stderr, encodeErr)
		return imagePath, encodeErr
	}

	imagePath = storageBaseUrl + "/" + filename

	req, reqErr := http.NewRequest("PUT", imagePath, data)
	if reqErr != nil {
		log.Println("Error: can not set request")
		log.Println(os.Stderr, reqErr)
		return imagePath, reqErr
	}
	fileExtWithDot := filepath.Ext(filename)
	fileExtNoDot := strings.Replace(fileExtWithDot, ".", "", -1)

	req.ContentLength = int64(data.Len())
	req.Header.Set("Date", time.Now().UTC().Format(http.TimeFormat))
	req.Header.Set("X-Amz-Acl", "public-read")
	req.Header.Set("Content-Type", "image/"+fileExtNoDot)
	s3.Sign(req, keys)

	_, doReqErr := http.DefaultClient.Do(req)
	if doReqErr != nil {
		log.Println(os.Stderr, doReqErr)
		return imagePath, doReqErr
	}

	return imagePath, nil
}

func PutImageToS3(theImage image.Image, sizeCode string, filename string) (string, error) {
	var imagePath string

	envErr := godotenv.Load()
	if envErr != nil {
		log.Println("Error: can not set S3 environment")
		log.Println(os.Stderr, envErr)
		return imagePath, envErr
	}

	keys := s3.Keys{
		AccessKey: os.Getenv("S3_ACCESS_KEY"),
		SecretKey: os.Getenv("S3_SECRET_KEY"),
	}

	data := new(bytes.Buffer)
	encodeErr := EncodeImage(theImage, filepath.Ext(filename), data)
	if encodeErr != nil {
		log.Println("Error: can not encode image")
		log.Println(os.Stderr, encodeErr)
		return imagePath, encodeErr
	}

	imagePath = storageBaseUrl + "/" + sizeCode + "/" + filename

	req, reqErr := http.NewRequest("PUT", imagePath, data)
	if reqErr != nil {
		log.Println("Error: can not set request")
		log.Println(os.Stderr, reqErr)
		return imagePath, reqErr
	}
	fileExtWithDot := filepath.Ext(filename)
	fileExtNoDot := strings.Replace(fileExtWithDot, ".", "", -1)

	req.ContentLength = int64(data.Len())
	req.Header.Set("Date", time.Now().UTC().Format(http.TimeFormat))
	req.Header.Set("X-Amz-Acl", "public-read")
	req.Header.Set("Content-Type", "image/"+fileExtNoDot)
	s3.Sign(req, keys)

	_, doReqErr := http.DefaultClient.Do(req)
	if doReqErr != nil {
		log.Println(os.Stderr, doReqErr)
		return imagePath, doReqErr
	}

	return imagePath, nil
}
