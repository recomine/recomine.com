package main

import (
	"log"
	"time"
)

type Feedback struct {
	Id            int64      `form:"-" db:"feedback_id" json:"id"`
	Created       int64      `form:"-" db:"created" json:"created"`
	Updated       int64      `form:"-" db:"updated" json:"updated"`
	UploaderId    int64      `form:"-" db:"uploader_id" json:"last_login"`
	Uploader      Recominer  `form:"-" db:"-" json:"uploader"`
	OwnerId       int64      `form:"-" db:"owner_id" json:"-"`
	Owner         Recominee  `form:"-" db:"-" json:"owner"`
	Message       string     `form:"message" db:"message" json:"message"`
	Public        bool       `form:"-" db:"public" json:"-"`
	Category      string     `form:"-" db:"category" json:"category"`
	Tags          string     `form:"-" db:"tags" json:"-"`
	InReplyTo     int64      `form:"-" db:"in_reply_to" json:"-"`
	Replies       []Feedback `form:"-" db:"-" json:"replies"`
	ImageId       int64      `form:"-" db:"image_id" json:"-"`
	FeedbackImage UserImage  `form:"-" db:"-" json:"image"`
}

func newFeedback(message string, fromWho int64, toWhom int64, replyingTo int64) Feedback {
	return Feedback{
		Message:    message,
		UploaderId: fromWho,
		OwnerId:    toWhom,
		InReplyTo:  replyingTo,
		Created:    time.Now().Unix(),
		Updated:    time.Now().Unix(),
		Public:     false,
	}
}

func (aFeedback *Feedback) UniqueId() interface{} {
	return aFeedback.Id
}

func (aFeedback *Feedback) GetById(id interface{}) error {
	err := dbmap.SelectOne(aFeedback, "SELECT * FROM feedback WHERE feedback_id = $1", id)
	if err != nil {
		return err
	}

	// _, err = dbmap.Select(&aFeedback.Replies, "SELECT * FROM feedback WHERE in_reply_to = $1 ORDER BY created", aFeedback.Id)
	// if err != nil {
	// 	return err
	// }

	return nil
}

func GetUploaderEmailOfFeedbackId(feedbackid interface{}) string {
	aFeedback := Feedback{}
	err := dbmap.SelectOne(&aFeedback, "select * from feedback where feedback_id = $1", feedbackid)
	if err != nil {
		log.Println(err.Error())
	}
	aRecominer := Recominer{}
	var returnedEmail string

	err = dbmap.SelectOne(&aRecominer, "select * from recominer where id = $1", aFeedback.UploaderId)
	if err != nil {
		log.Println(err.Error())
	} else {
		returnedEmail = aRecominer.Email
	}

	return returnedEmail
}

func fetchFeedbackRepliesFromFeedbackId(replyOf int64) (replies []Feedback) {
	_, err := dbmap.Select(&replies, "SELECT * FROM feedback WHERE in_reply_to = $1", replyOf)
	checkErr(err, "error fetching replies")

	for _, aReply := range replies {
		aReply.Uploader = fetchRecominerDetailFromId(aReply.UploaderId)
	}

	return
}
