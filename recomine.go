// Copyright 2014 Recomine.
// Recomine.com main website source code

package main

import (
	"code.google.com/p/go.crypto/bcrypt"
	"database/sql"
	"github.com/go-martini/martini"
	_ "github.com/lib/pq"
	// PRODUCTION NOTE: Uncomment this line below to activate go new relic library.
	//"github.com/martini-contrib/gorelic"
	"encoding/json"
	"github.com/coopernurse/gorp"
	"github.com/dgrijalva/jwt-go"
	"github.com/joho/godotenv"
	"github.com/martini-contrib/binding"
	"github.com/martini-contrib/gorelic"
	"github.com/martini-contrib/render"
	"github.com/martini-contrib/sessionauth"
	"github.com/martini-contrib/sessions"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"
)

var (
	privateKey []byte
	dbmap      *gorp.DbMap
)

func init() {
	privateKey, _ = ioutil.ReadFile("recomine.rsa")
}
func main() {
	envErr := godotenv.Load()
	if envErr != nil {
		log.Println("Error: Can not load environment.")
	}
	dbmap = initDb()
	store := sessions.NewCookieStore([]byte("B!sm1ll4h!rr@hm4n1rr4h!M"))

	server := martini.Classic()
	server.Use(martini.Static("assets"))
	server.Use(martini.Static("bower_components"))
	server.Use(martini.Recovery())
	server.Use(martini.Logger())

	// PRODUCTION NOTE: Uncomment the line below to set martini environment to production
	//martini.Env = martini.Prod
	//serverName := "recomine.com"
	//gorelic.InitNewrelicAgent("67daa13ac5be3c737c75a17ba77896334fdad5f1", serverName, true)
	//server.Use(gorelic.Handler)

	// PRODUCTION NOTE: Uncomment three lines below to activate go new relic library.

	server.Use(render.Renderer(render.Options{
		Directory: "templates",
		Funcs: []template.FuncMap{
			template.FuncMap{"lenplus": lenPlus, "formatTime": formatTime, "countCategory": countCategoryFromSlices},
		},
		Delims: render.Delims{"[[", "]]"},
	}))

	server.Use(sessions.Sessions("recomine", store))
	server.Use(sessionauth.SessionUser(GenerateAnonymousUser))

	server.Get("/", GetHome)

	server.Get("/login", GetLogin)
	server.Post("/login", binding.Bind(Recominer{}), PostLogin)

	server.Get("/dashboard", sessionauth.LoginRequired, GetDashboard)

	server.Get("/logout", sessionauth.LoginRequired, GetLogout)

	server.Get("/signup", GetSignup)
	server.Post("/signup", binding.Bind(Recominer{}), PostSignup)
	server.Get("/forgotpassword", GetForgotPassword)
	server.Post("/resetPassword", PostResetPassword)

	server.Group("/auth", func(router martini.Router) {
		router.Post("/login", PostAuthLogin)
		router.Post("/signup", PostAuthSignUp)
		router.Post("/signout", PostAuthSignOut)
		router.Post("/facebook", PostAuthFacebook)
		router.Post("/twitter", PostAuthTwitter)
		router.Post("/google", PostAuthGoogle)
	})

	server.Group("/me", func(router martini.Router) {
		router.Get("/:username", GetRecominer)
		router.Get("/:username/edit", sessionauth.LoginRequired, GetRecominerEdit)
		router.Post("/:username/edit", sessionauth.LoginRequired, binding.Bind(Recominer{}), PostRecominerEdit)

		router.Get("/:username/password", sessionauth.LoginRequired, GetChangePass)
		router.Post("/:username/password", sessionauth.LoginRequired, PostChangePass)
	})

	server.Group("/us", func(router martini.Router) {

		router.Get("/recominee", sessionauth.LoginRequired, GetRecomineeEdit)
		router.Post("/recominee", sessionauth.LoginRequired, PostRecomineeEdit)
		router.Get("/:recominee", GetRecominee)
		router.Get("/:recominee/edit", sessionauth.LoginRequired, GetRecomineeEdit)
		router.Post("/:recominee/feedback", sessionauth.LoginRequired, binding.Bind(Feedback{}), PostFeedback)
	})

	server.Group("/fb", func(router martini.Router) {
		router.Get("/:feedback", GetFeedback)
		router.Post("/:feedback/reply", PostFeedback)
	})

	server.Group("/api/v1", func(router martini.Router) {
		router.Get("/feedbacks", ApiGetFeedback)
		router.Post("/feedbacks", sessionauth.LoginRequired, ApiPostFeedback)
		router.Post("/:recominee/feedbacks", sessionauth.LoginRequired, ApiPostFeedback)

		router.Get("/feedbacks/:feedbackId", sessionauth.LoginRequired, ApiGetFeedbackById)
		router.Post("/feedbacks/:feedbackId", sessionauth.LoginRequired, ApiPostFeedbackReply)
		router.Get("/feedbacks/:feedbackId/replies", sessionauth.LoginRequired, ApiGetFeedbackReplies)

		router.Get("/us/feedbacks", sessionauth.LoginRequired, ApiGetRecomineeFeedback)
		router.Get("/me/feedbacks", sessionauth.LoginRequired, ApiGetRecominerFeedback)

		router.Get("/me/:recominer/usercheck", ApiGetCheckRecominer)
		router.Get("/me/usercheck", ApiGetCheckRecominer)

		router.Get("/me/:recominer/emailcheck", ApiGetEmailCheckRecominer)
		router.Get("/me/emailcheck", ApiGetEmailCheckRecominer)

		router.Get("/us/:recominee/usercheck", ApiGetCheckRecominee)
		router.Get("/us/usercheck", ApiGetCheckRecominee)

		router.Post("/us/upload", ApiPostUpload)
	})

	server.NotFound(func(renderTo render.Render) {
		renderTo.HTML(http.StatusOK, "notfound", nil)
	})

	server.Run()
}

// Initializing database using sql driver into gorp dbmap,
// adding tables and mapping it into the corresponding struct,
// if the table not exist, we will create it.

func initDb() *gorp.DbMap {
	db, err := sql.Open("postgres", "user=recomine password=R4h4s14sumed4ng dbname=recomine sslmode=disable")
	// TODO: implement a proper error checking
	checkErr(err, "sql.Open failed")

	dbmap := &gorp.DbMap{Db: db, Dialect: gorp.PostgresDialect{}}

	dbmap.AddTableWithName(Recominer{}, "recominer").SetKeys(true, "id")
	dbmap.AddTableWithName(Recominee{}, "recominee").SetKeys(true, "id")
	dbmap.AddTableWithName(Feedback{}, "feedback").SetKeys(true, "feedback_id")
	dbmap.AddTableWithName(UserImage{}, "userimage").SetKeys(true, "image_id")

	err = dbmap.CreateTablesIfNotExists()
	// TODO: implement a proper error checking
	// log.Println(err.Error())

	return dbmap
}

// Below is all of the functions used by net/http router

func GetHome(renderTo render.Render, recominer sessionauth.User) {
	if recominer.IsAuthenticated() {
		renderTo.Redirect("/dashboard")
		return
	} else {
		renderTo.HTML(http.StatusFound, "home", nil)
	}
}

func GetLogin(renderTo render.Render, recominer sessionauth.User) {
	if recominer.IsAuthenticated() {
		renderTo.Redirect("/dashboard")
		return
	} else {
		renderTo.HTML(http.StatusFound, "login", nil)
	}
}

func PostLogin(postedRecominer Recominer, req *http.Request, session sessions.Session, ferr binding.Errors, renderTo render.Render) {
	if ferr.Len() > 0 {
		newmap := map[string]interface{}{"alert": "Error with Form Submission"}
		renderTo.HTML(http.StatusFound, "login", newmap)
	} else {
		recominer := Recominer{}

		err := dbmap.SelectOne(&recominer, "SELECT * FROM recominer WHERE email = $1", postedRecominer.Email)

		if err != nil {
			if err == sql.ErrNoRows {
				renderTo.HTML(http.StatusFound, "login", map[string]interface{}{"alert": "email"})
			} else {
				checkErr(err, "error selecting recominer "+postedRecominer.Email)
				renderTo.Redirect(sessionauth.RedirectUrl)
			}
			return
		} else if bcrypt.CompareHashAndPassword([]byte(recominer.Password), []byte(postedRecominer.Password)) != nil {
			renderTo.HTML(http.StatusFound, "login", map[string]interface{}{"alert": "password"})
			return
		} else {
			err := sessionauth.AuthenticateSession(session, &recominer)
			checkErr(err, "Error authenticating session")
			recominer.Login()

			params := req.URL.Query()
			redirect := params.Get(sessionauth.RedirectParam)
			if len(redirect) > 0 {
				renderTo.Redirect(redirect, http.StatusFound)
			} else if len(req.FormValue("next")) > 0 {
				renderTo.Redirect(redirect)
			} else if req.FormValue("src") == "feedbox" {
				renderTo.Redirect("/us/" + req.FormValue("recominee"))
			} else {
				renderTo.Redirect("/dashboard")
			}
			return
		}
	}
}

func GetForgotPassword(req *http.Request, renderTo render.Render) {

}

func PostResetPassword(req *http.Request, renderTo render.Render) {

}

func PostAuthLogin(req *http.Request, renderTo render.Render) {
	var reqJSON interface{}

	err := json.NewDecoder(req.Body).Decode(&reqJSON)

	if err != nil {
		renderTo.JSON(http.StatusUnauthorized, map[string]interface{}{"message": "Can not read the json"})
		return
	}

	requestJSON := reqJSON.(map[string]interface{})

	if len(requestJSON["email"].(string)) <= 0 || len(requestJSON["password"].(string)) <= 0 {
		renderTo.JSON(http.StatusUnauthorized, map[string]interface{}{"message": "Please insert email and password."})
	} else {
		recominer := Recominer{}

		err := dbmap.SelectOne(&recominer, "SELECT email, password FROM recominer WHERE email = $1", requestJSON["email"])

		if err != nil {
			if err == sql.ErrNoRows {
				renderTo.JSON(http.StatusUnauthorized, map[string]interface{}{"message": "Wrong email"})
			} else {
				checkErr(err, "error selecting recominer")
				renderTo.JSON(http.StatusUnauthorized, map[string]interface{}{"message": "Wrong email"})
			}
			return
		} else if bcrypt.CompareHashAndPassword([]byte(recominer.Password), []byte(requestJSON["password"].(string))) != nil {
			renderTo.JSON(http.StatusUnauthorized, map[string]interface{}{"message": "Wrong password"})
			return
		} else {
			token := jwt.New(jwt.GetSigningMethod("HS256"))
			token.Claims["sub"] = recominer.Id
			token.Claims["name"] = recominer.Name
			token.Claims["full"] = recominer.FullName
			token.Claims["image"] = recominer.ProfileImageUrl
			token.Claims["iat"] = time.Now()
			token.Claims["exp"] = time.Now().Add(time.Hour * 24 * 14).Unix()
			tokenString, err := token.SignedString(privateKey)
			if err != nil {
				log.Println(err.Error())
				renderTo.JSON(http.StatusUnauthorized, map[string]interface{}{"message": "Can not parse token"})
				return
			}

			renderTo.JSON(http.StatusOK, map[string]interface{}{"token": tokenString})
			return
		}
	}
}

func PostAuthSignUp(renderTo render.Render, user sessionauth.User) {

}

func PostAuthSignOut(renderTo render.Render, user sessionauth.User) {

}

func PostAuthFacebook(renderTo render.Render, user sessionauth.User) {

}

func PostAuthTwitter(renderTo render.Render, user sessionauth.User) {

}

func PostAuthGoogle(renderTo render.Render, user sessionauth.User) {

}

func GetDashboard(renderTo render.Render, user sessionauth.User) {
	theRecominee := Recominee{}
	err := theRecominee.GetByMasterId(user.UniqueId())
	if err != nil {
		log.Println(err.Error())
	}
	renderTo.HTML(http.StatusOK, "dashboard", map[string]interface{}{"activeRecominer": user.(*Recominer), "theRecominee": theRecominee})
}

func GetLogout(session sessions.Session, request *http.Request, recominer sessionauth.User, renderTo render.Render) {
	sessionauth.Logout(session, recominer)
	log.Println(request.RemoteAddr)
	renderTo.Redirect("/")
}

func GetSignup(recominer sessionauth.User, renderTo render.Render) {
	if recominer.IsAuthenticated() {
		renderTo.Redirect("/dashboard")
		return
	} else {
		renderTo.HTML(http.StatusFound, "signup", nil)
	}
}

func PostSignup(postedRecominer Recominer, ferr binding.Errors, session sessions.Session, renderTo render.Render) {
	log.Println(ferr)

	if ferr.Len() > 0 {
		errorMap := map[string]interface{}{"alert": "Error with Form Submission"}
		renderTo.HTML(http.StatusFound, "signup", errorMap)
	} else {
		if IsUserNameDuplicate(postedRecominer.Name) {
			renderTo.HTML(http.StatusFound, "signup", map[string]interface{}{"alert": "username"})
		} else if IsEmailDuplicate(postedRecominer.Email) {
			renderTo.HTML(http.StatusFound, "signup", map[string]interface{}{"alert": "email"})
		} else {
			hashedPassword, err := bcrypt.GenerateFromPassword([]byte(postedRecominer.Password), bcrypt.DefaultCost)
			checkErr(err, "error hashing password")

			aNewRecominer := newRecominer(postedRecominer.Email, string(hashedPassword[:]), postedRecominer.Name, false)
			err = dbmap.Insert(&aNewRecominer)
			checkErr(err, "error inserting new user")

			err = dbmap.SelectOne(&aNewRecominer, "SELECT * FROM recominer WHERE email=$1", aNewRecominer.Email)
			checkErr(err, "error getting new recominer")
			aNewRecominer.LastLogin = time.Now().Unix()

			err = sessionauth.AuthenticateSession(session, &aNewRecominer)
			checkErr(err, "error authenticating user")

			renderTo.Redirect("/dashboard")
		}
	}
}

func GetRecominerEdit(authUser sessionauth.User, renderTo render.Render, params martini.Params) {
	aRecominer := Recominer{}
	err := aRecominer.GetById(authUser.UniqueId())
	checkErr(err, "error getting user detail by id")

	renderTo.HTML(http.StatusOK, "edituser", map[string]interface{}{"activeRecominer": aRecominer})
}

func PostRecominerEdit(postedRecominer Recominer, writer http.ResponseWriter, request *http.Request, authUser sessionauth.User, session sessions.Session, ferr binding.Errors, renderTo render.Render, params martini.Params) {
	if ferr.Len() > 0 {
		log.Println(ferr)
		errorMap := map[string]interface{}{"alertType": "danger", "alert": "Error with Form Submission"}
		renderTo.HTML(http.StatusFound, "edituser", errorMap)
	} else {
		aRecominer := Recominer{}
		err := aRecominer.GetByUserName(params["username"])
		if err != nil {
			renderTo.HTML(http.StatusFound, "edituser", map[string]interface{}{"alertType": "danger", "alert": "Error retrieving your username. Please try again or contact us if problem persist."})
		}

		if authUser.UniqueId() == aRecominer.UniqueId() {
			firstTimeCompleting := (len(aRecominer.FullName) == 0)

			aRecominer.FullName = postedRecominer.FullName
			aRecominer.Sex = postedRecominer.Sex
			aRecominer.Address = postedRecominer.Address
			aRecominer.Phone = postedRecominer.Phone
			aRecominer.Birthplace = postedRecominer.Birthplace
			aRecominer.Birthday = postedRecominer.Birthday
			aRecominer.Bio = postedRecominer.Bio
			aRecominer.Facebook = postedRecominer.Facebook
			aRecominer.Twitter = postedRecominer.Twitter
			aRecominer.Updated = time.Now().Unix()

			// handle the file upload

			file, _, err := request.FormFile("inputPicture")

			if err != nil {
				log.Println("Error: can not retrieve file")
			} else {
				defer file.Close()
				aNewImage := newUserImage(aRecominer.Id, 0)
				err = dbmap.Insert(&aNewImage)
				if err == nil {
					imagePath, err := UploadImageOfSizes(file, aRecominer.Id, aNewImage.Id, picoSize, nanoSize, tinySize)
					if err != nil {
						log.Println("Error: can not upload image")
					} else {
						aRecominer.ProfileImageUrl = imagePath
						aNewImage.Url = imagePath
						_, err = dbmap.Update(&aNewImage)
						if err != nil {
							log.Println("Error: can not update image URL")
						}
					}
				}
			}

			count, err := dbmap.Update(&aRecominer)
			if err != nil {
				log.Println(err.Error())
			}
			log.Println("Users rows updated:", count)

			if firstTimeCompleting {
				renderTo.Redirect("/dashboard")
			} else {
				renderTo.HTML(http.StatusFound, "edituser", map[string]interface{}{"activeRecominer": aRecominer, "alertType": "success", "alert": "You have successfully updated your profile.", "firstTime": firstTimeCompleting})
			}
		}
	}
}

func GetChangePass(authUser sessionauth.User, session sessions.Session, renderTo render.Render, params martini.Params) {
	recominer := Recominer{}
	err := recominer.GetById(authUser.UniqueId())
	checkErr(err, "error getting user by id")
	renderTo.HTML(http.StatusFound, "editpassword", map[string]interface{}{"activeRecominer": recominer})
}

func PostChangePass(authUser sessionauth.User, req *http.Request, session sessions.Session, renderTo render.Render, params martini.Params) {
	oldPassword, password, passwordRetype := req.FormValue("old_password"), req.FormValue("password"), req.FormValue("password_retype")

	aRecominer := Recominer{}
	err := aRecominer.GetByUserName(params["username"])
	checkErr(err, "error getting recominer by username")

	if isPasswordCorrect(oldPassword, authUser.UniqueId()) {
		renderTo.HTML(http.StatusFound, "editpassword", map[string]interface{}{"activeRecominer": aRecominer, "alert": "Oops, you entered a wrong old password."})
		return
	} else if password != passwordRetype {
		renderTo.HTML(http.StatusFound, "editpassword", map[string]interface{}{"activeRecominer": aRecominer, "alert": "Oops, you entered different new passwords."})
		return
	} else {
		hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
		checkErr(err, "error hashing password")

		if authUser.UniqueId() == aRecominer.UniqueId() {
			aRecominer.Password = string(hashedPassword[:])
			aRecominer.Updated = time.Now().Unix()
			count, err := dbmap.Update(&aRecominer)
			checkErr(err, "Update user failed")
			log.Println("Rows updated:", count)

			renderTo.HTML(http.StatusFound, "editpassword", map[string]interface{}{"activeRecominer": aRecominer, "alert": "Successfully changed your password."})
		}
	}
}

func GetRecominer(authUser sessionauth.User, session sessions.Session, renderTo render.Render, params martini.Params) {
	viewedRecominer := Recominer{}
	err := viewedRecominer.GetByUserName(params["username"])
	if err == sql.ErrNoRows {
		renderTo.Status(http.StatusNotFound)
		return
	}

	var activeRecominer Recominer
	if authUser.IsAuthenticated() {
		err := activeRecominer.GetById(authUser.UniqueId())
		checkErr(err, "error getting recominer detail")
	}

	renderTo.HTML(200, "recominer", map[string]interface{}{"activeRecominer": activeRecominer, "viewedRecominer": viewedRecominer})
}

func GetRecominee(authUser sessionauth.User, session sessions.Session, renderTo render.Render, params martini.Params) {
	viewedRecominee := Recominee{}
	err := viewedRecominee.GetByUserName(params["recominee"])

	if err == sql.ErrNoRows {
		renderTo.Status(http.StatusNotFound)
		return
	}

	var activeRecominer Recominer
	if authUser.IsAuthenticated() {
		err := activeRecominer.GetById(authUser.UniqueId())
		checkErr(err, "error getting recominer detail")
	}

	renderTo.HTML(http.StatusFound, "recominee", map[string]interface{}{"activeRecominer": activeRecominer, "viewedRecominee": viewedRecominee})
}

func GetFeedback(authUser sessionauth.User, session sessions.Session, renderTo render.Render, params martini.Params) {
	renderTo.Redirect("/dashboard")
}

func PostFeedback(postedFeedback Feedback, authUser sessionauth.User, request *http.Request, session sessions.Session, renderTo render.Render, params martini.Params) {
	activeRecominer := Recominer{}
	err := activeRecominer.GetById(authUser.UniqueId())
	if err != nil {
		log.Print(err.Error())
	}

	viewedRecominee := Recominee{}
	err = viewedRecominee.GetByUserName(params["recominee"])
	if err != nil {
		log.Print(err.Error())
	}

	aNewFeedback := newFeedback(postedFeedback.Message, activeRecominer.Id, viewedRecominee.Id, 0)

	if request.FormValue("complaint") == "on" {
		aNewFeedback.Category = "complaint"
	} else if request.FormValue("compliment") == "on" {
		aNewFeedback.Category = "compliment"
	} else if request.FormValue("question") == "on" {
		aNewFeedback.Category = "question"
	} else {
		aNewFeedback.Category = "suggestion"
	}

	err = dbmap.Insert(&aNewFeedback)
	if err != nil {
		log.Print(err.Error())
	}
	renderTo.HTML(http.StatusOK, "recominee", map[string]interface{}{"activeRecominer": activeRecominer, "viewedRecominee": viewedRecominee, "alert": "Posting feedback success!"})
}

func PostFeedbackReply(postedFeedback Feedback, authUser sessionauth.User, request *http.Request, session sessions.Session, renderTo render.Render, params martini.Params) {
	if authUser.IsAuthenticated() {
		activeRecominer := Recominer{}
		err := activeRecominer.GetById(authUser.UniqueId())
		checkErr(err, "error getting recominer detail")

		viewedRecominee := Recominee{}
		err = viewedRecominee.GetByUserName(params["recominee"])
		checkErr(err, "error getting recominee detail")

		feedbackID, err := strconv.ParseInt(params["feedback"], 10, 64)
		checkErr(err, "error converting to integer")
		aNewFeedback := newFeedback(postedFeedback.Message, activeRecominer.Id, viewedRecominee.Id, feedbackID)

		if request.FormValue("complaint") == "on" {
			aNewFeedback.Category = "complaint"
		} else if request.FormValue("compliment") == "on" {
			aNewFeedback.Category = "compliment"
		} else if request.FormValue("question") == "on" {
			aNewFeedback.Category = "question"
		} else {
			aNewFeedback.Category = "suggestion"
		}

		err = dbmap.Insert(&aNewFeedback)
		checkErr(err, "error inserting feedback")
		renderTo.HTML(http.StatusFound, "recominee", map[string]interface{}{"activeRecominer": activeRecominer, "viewedRecominee": viewedRecominee, "alert": "Posting feedback success!"})
	} else {
		// TODO: handle authentication
		log.Println("Not Authenticated")
	}
}

func GetRecomineeEdit(renderTo render.Render, authUser sessionauth.User, params martini.Params) {
	if len(params["recominee"]) == 0 {
		activeRecominer := Recominer{}
		err := activeRecominer.GetById(authUser.UniqueId())
		checkErr(err, "error getting recominer detail")

		activeRecominee := Recominee{}
		err = activeRecominee.GetByMasterId(authUser.UniqueId())
		if err != nil && err != sql.ErrNoRows {
			checkErr(err, "error getting recominee")
		}

		renderTo.HTML(http.StatusFound, "editrecominee", map[string]interface{}{"activeRecominer": activeRecominer, "activeRecominee": activeRecominee})
	} else {
		activeRecominer := Recominer{}
		err := activeRecominer.GetById(authUser.UniqueId())
		if err != nil && err != sql.ErrNoRows {
			checkErr(err, "error getting recominer detail")
		}

		activeRecominee := Recominee{}
		err = activeRecominee.GetByUserName(params["recominee"])
		if err != nil && err != sql.ErrNoRows {
			checkErr(err, "error getting recominee")
		}

		renderTo.HTML(http.StatusFound, "editrecominee", map[string]interface{}{"activeRecominer": activeRecominer, "activeRecominee": activeRecominee})
	}
}

func PostRecomineeEdit(renderTo render.Render, authUser sessionauth.User, request *http.Request) {
	activeRecominer := Recominer{}
	existingRecominee := Recominee{}

	err := activeRecominer.GetById(authUser.UniqueId())
	if err != nil {
		log.Println("Error: can not retrieve recominer detail")
		existingRecominee.ShortName = strings.Replace(request.FormValue("businessShortName"), " ", "", -1)
		existingRecominee.FullName = request.FormValue("businessFullName")
		existingRecominee.Description = request.FormValue("businessDescription")
		existingRecominee.Address = request.FormValue("businessAddress")
		existingRecominee.Phone = request.FormValue("businessPhone")
		existingRecominee.Website = request.FormValue("businessWebsite")
		renderTo.HTML(http.StatusOK, "editrecominee", map[string]interface{}{"activeRecominer": activeRecominer, "activeRecominee": existingRecominee, "alertType": "danger", "alert": "Sorry. There's something wrong. Failed updating your business. Can not retrieve recominer detail. Please refresh your page."})
		return
	}

	existingRecomineeErr := existingRecominee.GetByMasterId(authUser.UniqueId())

	if existingRecomineeErr != nil && existingRecomineeErr != sql.ErrNoRows {
		log.Println("Error: can not retrieve existing recominee")
		renderTo.HTML(http.StatusOK, "editrecominee", map[string]interface{}{"activeRecominer": activeRecominer, "activeRecominee": existingRecominee, "alertType": "danger", "alert": "Sorry. There's something wrong. Failed updating your business. Can not retrive recominee detail."})
		return
	} else if existingRecomineeErr == sql.ErrNoRows {
		registeredShortName := strings.Replace(request.FormValue("businessShortName"), " ", "", -1)
		if IsBusinessShortNameDuplicate(0, registeredShortName) {
			log.Println("Error: duplicate business short name")
			existingRecominee.FullName = request.FormValue("businessFullName")
			existingRecominee.Description = request.FormValue("businessDescription")
			existingRecominee.Address = request.FormValue("businessAddress")
			existingRecominee.Phone = request.FormValue("businessPhone")
			existingRecominee.Website = request.FormValue("businessWebsite")
			renderTo.HTML(http.StatusOK, "editrecominee", map[string]interface{}{"activeRecominer": activeRecominer, "activeRecominee": existingRecominee, "alertType": "danger", "alert": "Sorry. The business short name (" + registeredShortName + ") is already taken. Please change to another one."})
			return
		}

		if len(request.FormValue("businessShortName")) > 1 || len(request.FormValue("businessShortName")) < 31 {

			aNewRecominee := newRecominee(strings.Replace(request.FormValue("businessShortName"), " ", "", -1), activeRecominer.Id)
			aNewRecominee.FullName = request.FormValue("businessFullName")
			aNewRecominee.Description = request.FormValue("businessDescription")
			aNewRecominee.Address = request.FormValue("businessAddress")
			aNewRecominee.Phone = request.FormValue("businessPhone")
			aNewRecominee.Website = request.FormValue("businessWebsite")
			// handle the file upload

			file, _, err := request.FormFile("inputPicture")

			if err != nil {
				log.Println("Error: can not retrieve file")
			} else {
				defer file.Close()
				aNewImage := newUserImage(aNewRecominee.Id, 0)
				err = dbmap.Insert(&aNewImage)
				if err == nil {
					imagePath, err := UploadImageOfSizes(file, aNewRecominee.Id, aNewImage.Id, picoSize, nanoSize, tinySize)
					if err != nil {
						log.Println("Error: can not upload image")
					} else {
						aNewRecominee.ProfileImageUrl = imagePath
						aNewImage.Url = imagePath
						_, err = dbmap.Update(&aNewImage)
						if err != nil {
							log.Println("Error: can not update image URL")
						}
					}
				}
			}

			err = dbmap.Insert(&aNewRecominee)
			if err != nil {
				log.Println(err.Error())
				log.Println("Error: can not insert new recominee to database")
				renderTo.HTML(http.StatusOK, "editrecominee", map[string]interface{}{"activeRecominer": activeRecominer, "activeRecominee": existingRecominee, "alertType": "danger", "alert": "Sorry. There's something wrong. Failed updating your business. Can not insert the new recominee."})
				return
			}

			err = aNewRecominee.GetByMasterId(activeRecominer.Id)
			if err != nil {
				log.Println("Error: can not retrieve recominee from database")
			}

			renderTo.HTML(http.StatusOK, "editrecominee", map[string]interface{}{"activeRecominer": activeRecominer, "activeRecominee": aNewRecominee, "alertType": "success", "alert": "Succesfully added your business."})
		} else {
			renderTo.HTML(http.StatusOK, "editrecominee", map[string]interface{}{"activeRecominer": activeRecominer, "activeRecominee": existingRecominee, "alertType": "danger", "alert": "Sorry. Failed updating your business profile. Business short name is minimum 2 and maximum 30."})
		}
	} else {
		registeredShortName := strings.Replace(request.FormValue("businessShortName"), " ", "", -1)

		if IsBusinessShortNameDuplicate(existingRecominee.Id, registeredShortName) {
			log.Println("Error: duplicate business short name")
			existingRecominee.FullName = request.FormValue("businessFullName")
			existingRecominee.Description = request.FormValue("businessDescription")
			existingRecominee.Address = request.FormValue("businessAddress")
			existingRecominee.Phone = request.FormValue("businessPhone")
			existingRecominee.Website = request.FormValue("businessWebsite")
			renderTo.HTML(http.StatusOK, "editrecominee", map[string]interface{}{"activeRecominer": activeRecominer, "activeRecominee": existingRecominee, "alertType": "danger", "alert": "Sorry. The business short name (" + registeredShortName + ") is already taken. Please change to another one."})
			return
		}

		if len(request.FormValue("businessShortName")) > 1 || len(request.FormValue("businessShortName")) < 31 {
			existingRecominee.ShortName = request.FormValue("businessShortName")
			existingRecominee.FullName = request.FormValue("businessFullName")
			existingRecominee.Description = request.FormValue("businessDescription")
			existingRecominee.Address = request.FormValue("businessAddress")
			existingRecominee.Phone = request.FormValue("businessPhone")
			existingRecominee.Website = request.FormValue("businessWebsite")

			// handle the file upload

			file, _, err := request.FormFile("inputPicture")

			if err != nil {
				log.Println("Error: can not retrieve file")
			} else {
				defer file.Close()
				aNewImage := newUserImage(existingRecominee.Id, 0)
				err = dbmap.Insert(&aNewImage)
				if err == nil {
					imagePath, err := UploadImageOfSizes(file, existingRecominee.Id, aNewImage.Id, picoSize, nanoSize, tinySize)
					if err != nil {
						log.Println("Error: can not upload image")
					} else {
						existingRecominee.ProfileImageUrl = imagePath
						aNewImage.Url = imagePath
						_, err = dbmap.Update(&aNewImage)
						if err != nil {
							log.Println("Error: can not update image URL")
						}
					}
				}
			}

			existingRecominee.Updated = time.Now().Unix()

			_, err = dbmap.Update(&existingRecominee)
			if err != nil {
				log.Println("Error: can not updating existing recominee")
				renderTo.HTML(http.StatusOK, "editrecominee", map[string]interface{}{"activeRecominer": activeRecominer, "activeRecominee": existingRecominee, "alertType": "danger", "alert": "Sorry. There's something wrong. Failed updating your business. Can not update the recominee"})
				return
			}

			err = existingRecominee.GetByMasterId(activeRecominer.Id)
			if err != nil {
				log.Println("Error: can not retrieve existing recominee")
			}

			renderTo.HTML(http.StatusOK, "editrecominee", map[string]interface{}{"activeRecominer": activeRecominer, "activeRecominee": existingRecominee, "alertType": "success", "alert": "Succesfully updated your business."})
		} else {
			renderTo.HTML(http.StatusOK, "editrecominee", map[string]interface{}{"activeRecominer": activeRecominer, "activeRecominee": existingRecominee, "alertType": "danger", "alert": "Sorry. Updating your business profile failed. Business short name is minimum 2 and maximum 30."})
		}
	}
}
