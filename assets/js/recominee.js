(function() {
    var feedboxApp = angular.module('feedbox', ['auth0'])
    .config(function (authProvider) {
      authProvider.init({
        domain: 'recomine.auth0.com',
        clientID: 'qTfuzkseSWX9FVNZt1OlOviB0SbPRQIw',
        callbackURL: location.href
      });
    })
    .run(function(auth) {
      // This hooks al auth events to check everything as soon as the app starts
      auth.hookEvents();
    });

    feedboxApp.controller('LoginController',function($scope, auth, $http){
        $scope.login = function() {
            auth.signin({
            popup: true
          }, function() {
            // Success callback
            console.log("logged in")
          }, function() {
            // Error callback
            console.log("error logging in")
          });
        }
    });
})();
