(function() {
    angular.module('feedbox', ['satellizer'])
    .controller('LoginController', function($scope, $auth) {
    	$scope.userName = {};
    	$scope.fullName = {};
    	$scope.userImageUrl = {};
    	$scope.payload = {};

    	$scope.isAuthenticated = function() {
		  	return $auth.isAuthenticated();
		};

		$scope.payload = function() {
    		return $auth.getPayload();
    	};

    	if ($scope.isAuthenticated()) {
			console.log($scope.payload());

      		// $scope.userName = payload.name;

      		// if (payload.full.length() > 0) {
      		// 	$scope.fullName = payload.full;
      		// } else {
      		// 	$scope.fullName = payload.name;
      		// }
      		
      		// if (payload.image.length() > 0) {
      		// 	$scope.userImageUrl = "http://media.recomine.com/p/" + payload.image;
      		// } else {
      		// 	$scope.userImageUrl = "//" + location.host + "/images/personal20.png";
      		// }
    	};

        $scope.login = function() {
          $auth.login({email: $scope.email, password: $scope.password})
          	.then(function() {
          		// payload = $auth.getPayload();
          		// $scope.userName = payload.name;

          		// if (payload.full.length() > 0) {
          		// 	$scope.fullName = payload.full;
          		// } else {
          		// 	$scope.fullName = payload.name;
          		// }

          		// if (payload.image.length() > 0) {
          		// 	$scope.userImageUrl = "http://media.recomine.com/p/" + payload.image;
          		// } else {
          		// 	$scope.userImageUrl = "//" + location.host + "/images/personal20.png";
          		// }
          		
          		location.reload();
	        })
	        .catch(function(response) {
	          	// console.log(response.data.message);
	        });
        };
    });
})();
