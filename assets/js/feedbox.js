$(function() {
    'use strict';
    var destination;
    if (($('#destination').val()).length > 0) {
        destination = $('#destination').val() + "/";
    }
    // Initialize the jQuery File Upload widget:
    $('#feedbox-form').fileupload({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        url: '/api/v1/'+destination+'feedbacks'
    });

    // Enable iframe cross-domain access via redirect option:
    $('#fileupload').fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        )
    );

    if (window.location.hostname === 'localhost:2500') {
        // Demo settings:
        $('#feedbox-form').fileupload('option', {
            url: '//recomine/api/v1/'+destination+'feedbacks',
            // Enable image resizing, except for Android and Opera,
            // which actually support image resizing, but fail to
            // send Blob objects via XHR requests:
            disableImageResize: /Android(?!.*Chrome)|Opera/
                .test(window.navigator.userAgent),
            maxFileSize: 5000000,
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
            autoUpload: false
        });
        // Upload server status check for browsers with CORS support:
        if ($.support.cors) {
            $.ajax({
                url: '//localhost:2500/',
                type: 'HEAD'
            }).fail(function () {
                $('<div class="alert alert-danger"/>')
                    .text('Upload server currently unavailable - ' +
                            new Date())
                    .appendTo('#fileupload');
            });
        }
    } else {
        // Load existing files:
        $('#feedbox-form').addClass('fileupload-processing');
        $.ajax({
            // Uncomment the following to send cross-domain cookies:
            //xhrFields: {withCredentials: true},
            url: $('#feedbox-form').fileupload('option', 'url'),
            dataType: 'json',
            context: $('#feedbox-form')[0]
        }).always(function () {
            $(this).removeClass('fileupload-processing');
        }).done(function (result) {
            $(this).fileupload('option', 'done')
                .call(this, $.Event('done'), {result: result});
        });
    }

    $('#loginSendButton').click(function(event) {
        $('#signupModal').modal('hide');
        $('#loginModal').modal();
    });

    $('#loginButton').click(function(event) {
        $('#signupModal').modal('hide');
        $('#loginModal').modal();
    });

    $('#signupButton').click(function(event) {
        $('#loginModal').modal('hide');
        $('#signupModal').modal();
    });

    $('#shareme').sharrre({
      share: {
        googlePlus: true,
        facebook: true,
        twitter: true,
        linkedin: true
      },
      buttons: {
        googlePlus: {size: 'tall', annotation:'bubble'},
        facebook: {layout: 'box_count'},
        twitter: {count: 'vertical'},
        linkedin: {counter: 'top'}
      },
      enableHover: false,
      enableCounter: false,
      enableTracking: true
    });
});
