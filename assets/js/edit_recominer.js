(function($) {
    $.fn.extend( {
        limiter: function(limit, elem) {
            $(this).on("keyup focus", function() {
                setCount(this, elem);
            });
            function setCount(src, elem) {
                var chars = src.value.length;
                if (chars > limit) {
                    src.value = src.value.substr(0, limit);
                    chars = limit;
                }
                elem.html( limit - chars );
            }
            setCount($(this)[0], elem);
        }
    });
})(jQuery);

$(document).ready(function() {
    var initialName = $("#inputName")[0].defaultValue;
    var initialEmail = $("#inputEmail")[0].defaultValue;
    
    var bioCount = $("#bioCount");
    $("#inputBio").limiter(150, bioCount);

    $('#editRecominerForm').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            inputPicture: {
                message: 'The file is not valid',
                validators: {
                    file: {
                        type: 'image/jpeg,image/png,image/gif',
                        extension: 'jpeg,jpg,png,gif',
                        maxSize: 2048 * 1024,
                        message: 'You can only upload image of type jpg, png, or gif with maximum size of 2MB.'
                    }
                }
            },
            name: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'The user name is required and cannot be empty'
                    },
                    stringLength: {
                        min: 2,
                        max: 30,
                        message: 'The user name must be more than 2 and less than 30 characters long'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9]+$/,
                        message: 'The user name can only consist of alphabetical and number'
                    },
                    remote: {
                        message: 'The user name is already taken. Please choose another one.',
                        url: '/api/v1/me/usercheck',
                        data: {
                            initialName: initialName
                        },
                        delay: 1000
                    }
                }
            },
            email: {
                message: 'The email is not valid',
                validators: {
                    notEmpty: {
                        message: 'The email is required and cannot be empty'
                    },
                    emailAddress: {
                        message: 'Please enter a valid email'
                    },
                    remote: {
                        message: 'The email is already taken. Please choose another one.',
                        url: '/api/v1/me/emailcheck',
                        data: {
                            initialEmail: initialEmail
                        },
                        delay: 1000
                    }
                }
            },
            fullname: {
                message: 'The full name is not valid',
                validators: {
                    notEmpty: {
                        message: 'The full name is required and cannot be empty'
                    },
                    stringLength: {
                        max: 50,
                        message: 'The full name must be less than 50 characters long'
                    }
                }
            },
            bio: {
                message: 'The bio is not valid',
                validators: {
                    stringLength: {
                        max: 150,
                        message: 'The bio must be less than 150 characters long.'
                    }
                }
            },
            phone: {
                message: 'The phone number is not valid',
                validators: {
                    numeric: {
                        message: 'The phone must be consist of only number.'
                    }
                }
            },
            address: {
                message: 'The address is not valid',
                validators: {
                    stringLength: {
                        max: 100,
                        message: 'The address must be less than 100 characters long.'
                    }
                }
            }
        }
    }); 
});
