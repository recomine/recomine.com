(function() {
    var app = angular.module('recomine', ['angularMoment']);

    app.controller('DashboardController', function($scope, $http) {
        $scope.itemType = 'Personal';
        $scope.feedbacks = [];

        $scope.setItemType = function(theItem) {
            $scope.itemType = theItem;
            var url = 'Personal';

            if (theItem == 'Personal') {
                url = '/api/v1/me/feedbacks';
            } else if (theItem == 'Business') {
                url = '/api/v1/us/feedbacks';
            }

            $http.get(url).success(function(data) {
                $scope.feedbacks = data;
                //console.log(data);
            });
        };

        $scope.setItemType($scope.itemType);

        $scope.replies = [];
        $scope.feedbacks.forEach(function(element) {
            $scope.replies.push({
                reply: '',
            });
        });

        this.addReply = function(feedbackID, aReply, inputIndex) {
            console.log(feedbackID + "-" + aReply + "-" + inputIndex);
            var responsePromise = $http.post('/api/v1/feedbacks/' + feedbackID, {
                'message': aReply
            });
            responsePromise.success(function(data) {
                //console.log(data.data);
                var newfeedback = data.data;
                $scope.feedbacks.forEach(function(element) {
                    if (element.id == newfeedback.in_reply_to) {
                        element.replies.push(newfeedback);
                    }
                });
            });
            responsePromise.error(function(data) {
                console.log("error posting feedback reply");
            });

            $scope.replies[inputIndex] = {
                reply: '',
            };
        };
    });
})();
