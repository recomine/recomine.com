(function($) {
    $.fn.extend( {
        limiter: function(limit, elem) {
            $(this).on("keyup focus", function() {
                setCount(this, elem);
            });
            function setCount(src, elem) {
                var chars = src.value.length;
                if (chars > limit) {
                    src.value = src.value.substr(0, limit);
                    chars = limit;
                }
                elem.html( limit - chars );
            }
            setCount($(this)[0], elem);
        }
    });
})(jQuery);

$(document).ready(function() {
    var initialName = $("#inputBusinessShortName")[0].defaultValue;

    var busDescCount = $("#businessDescriptionCount");  
    $("#inputBusinessDescription").limiter(150, busDescCount);

    $('#signupForm').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'The username is required and cannot be empty'
                    },
                    stringLength: {
                        min: 2,
                        max: 30,
                        message: 'The username must be more than 2 and less than 30 characters long'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9]+$/,
                        message: 'The username can only consist of alphabetical and number'
                    },
                    different: {
                        field: 'password',
                        message: 'The username and password cannot be the same as each other'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required and cannot be empty'
                    },
                    emailAddress: {
                        message: 'The email address is not a valid'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'The password is required and cannot be empty'
                    },
                    different: {
                        field: 'username',
                        message: 'The password cannot be the same as username'
                    },
                    stringLength: {
                        min: 8,
                        message: 'The password must have at least 8 characters'
                    }
                }
            }
        }
    });

    $('#loginForm').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            email: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required and cannot be empty'
                    },
                    emailAddress: {
                        message: 'The email address is not a valid'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'The password is required and cannot be empty'
                    },
                    stringLength: {
                        min: 8,
                        message: 'The password must have at least 8 characters'
                    }
                }
            }
        }
    });

    $('#editRecomineeForm').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            inputPicture: {
                message: 'The file is not valid',
                validators: {
                    file: {
                        type: 'image/jpeg,image/png,image/gif',
                        extension: 'jpeg,jpg,png,gif',
                        maxSize: 2048 * 1024,
                        message: 'You can only upload image of type jpg, png, or gif with maximum size of 2MB.'
                    }
                }
            },
            businessShortName: {
                message: 'The business name is not valid',
                validators: {
                    notEmpty: {
                        message: 'The business name is required and cannot be empty'
                    },
                    stringLength: {
                        min: 2,
                        max: 30,
                        message: 'The business name must be more than 2 and less than 30 characters long'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9]+$/,
                        message: 'The business name can only consist of alphabetical and number'
                    },
                    remote: {
                        message: 'The short name is already taken. Please choose another one.',
                        url: '/api/v1/us/usercheck',
                        data: {
                            initialName: initialName
                        },
                        delay: 1000
                    }
                }
            },
            businessFullName: {
                message: 'The business full name is not valid',
                validators: {
                    notEmpty: {
                        message: 'The business full name is required and cannot be empty'
                    },
                    stringLength: {
                        max: 50,
                        message: 'The business full name must be less than 50 characters long'
                    }
                }
            },
            businessDescription: {
                message: 'The business description is not valid',
                validators: {
                    stringLength: {
                        max: 150,
                        message: 'The business description must be less than 150 characters long.'
                    }
                }
            },
            businessPhone: {
                message: 'The business phone is not valid',
                validators: {
                    numeric: {
                        message: 'The business phone must be consist of only number.'
                    }
                }
            },
            businessAddress: {
                message: 'The business address is not valid',
                validators: {
                    stringLength: {
                        max: 100,
                        message: 'The business address must be less than 100 characters long.'
                    }
                }
            },
            businessWebsite:{
                message: 'The business website address is not valid',
                validators: {
                    urinoprotocol: {
                        message: 'The business website address is not valid.'
                    }
                }
            }
        }
    });

    $('#editPassForm').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            password: {
                validators: {
                    identical: {
                        field: 'password_retype',
                        message: 'The password and its confirm are not the same'
                    },
                    stringLength: {
                        min: 8,
                        message: 'The password must have at least 8 characters'
                    }
                }
            },
            password_retype: {
                validators: {
                    identical: {
                        field: 'password',
                        message: 'The password and its confirm are not the same'
                    },
                    stringLength: {
                        min: 8,
                        message: 'The password must have at least 8 characters'
                    }
                }
            }
        }
    });

    
});

// $(function () {
//     $('#editRecomineeForm').fileupload({
//         dataType: 'json',
//         add: function (e, data) {
//             data.context = $('#submitButton')
//                 .click(function () {
//                     data.context = $('#submitButton').text('Uploading...').replaceAll($(this));
//                     $('#upload-progress').removeClass("hidden");
//                     data.submit();
//                 });
//         },
//         progressall: function (e, data) {
//             var progress = parseInt(data.loaded / data.total * 100, 10);
//             console.log(progress);
//             $('#upload-progress .progress-bar').css(
//                 'width',
//                 progress + '%'
//             );
//         },
//         done: function (e, data) {
//             data.context.text('Upload finished');
//         }
//     });
// });
