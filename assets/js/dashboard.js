$(document).ready(function() {
    $('#recoselect').ddslick({
        onSelected: function(data) {
            //console.log(data.selectedData.description);

            var scope = angular.element($("body")).scope();

            scope.$apply(function(){
                scope.setItemType(data.selectedData.description);
            });
        },
        width: 300,
        background: "#fff",
    });
    
    $('#recoselectmini').ddslick({
        onSelected: function(data) {
            //console.log(data.selectedData.description);

            var scope = angular.element($("body")).scope();

            scope.$apply(function(){
                scope.setItemType(data.selectedData.description);
            });
        },
        width: 300,
        background: "#fff",
    });

    $('.growing-text').autogrow();

    var client = new ZeroClipboard(document.getElementById("copyLinkButton"));

    client.on("ready", function(readyEvent) {
        client.on("aftercopy", function(event) {
            $('#linkWell').tooltip({
                placement: 'right',
                title: 'Copied!'
            });
            $('#linkWell').tooltip('toggle');
        });
    });
    // $('.growing-text').autogrow(); 

    // $('.feedback-thumbnail').on(
    //     'click': function() {
    //         var feedbackUrl = $this.attr("id");
    //         console.log(feedbackUrl);
    //         $('#imageViewer').attr('src',"http://media.recomine.com/m/"+feedbackUrl);
    //         $('#thumbnailModal').modal();
    //     }
    // );
    
});


