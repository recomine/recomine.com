package main

import (
	"database/sql"
	"log"
	"time"
)

type Recominee struct {
	Id              int64  `form:"-" db:"id" json:"id"`
	ShortName       string `form:"shortname" db:"short_name" json:"short_name"`
	FullName        string `form:"fullname" db:"full_name" json:"full_name"`
	Description     string `form:"description" db:"description" json:"description"`
	Created         int64  `form:"-" db:"created" json:"created"`
	Updated         int64  `form:"-" db:"updated" json:"-"`
	Phone           string `form:"phone" db:"phone" json:"-"`
	Address         string `form:"address" db:"address" json:"-"`
	Website         string `form:"website" db:"website" json:"-"`
	Admin           int64  `form:"-" db:"admin" json:"-"`
	Master          int64  `form:"-" db:"master" json:"-"`
	ProfileImageUrl string `form:"-" db:"profile_image_url" json:"profile_image_url"`
	HeaderImageUrl  string `form:"-" db:"header_image_url" json:"header_image_url"`
}

func newRecominee(shortname string, master int64) Recominee {
	return Recominee{
		ShortName: shortname,
		Master:    master,
		Created:   time.Now().Unix(),
		Updated:   time.Now().Unix(),
	}
}

func (recominee *Recominee) UniqueId() interface{} {
	return recominee.Id
}

func (recominee *Recominee) GetById(id interface{}) error {
	err := dbmap.SelectOne(recominee, "SELECT * FROM recominee WHERE id = $1", id)
	if err != nil {
		return err
	}

	return nil
}

func (recominee *Recominee) GetByMasterId(id interface{}) error {
	err := dbmap.SelectOne(recominee, "SELECT * FROM recominee WHERE master = $1", id)
	if err != nil {
		return err
	}

	return nil
}

func (recominee *Recominee) GetByUserName(shortname interface{}) error {
	err := dbmap.SelectOne(recominee, "SELECT * FROM recominee WHERE short_name = $1", shortname)
	if err != nil {
		return err
	}
	return nil
}

func IsBusinessShortNameDuplicate(currentBusinessId int64, shortname string) bool {
	var recominees []Recominee
	var err error

	if currentBusinessId == 0 {
		_, err = dbmap.Select(&recominees, "SELECT * FROM recominee WHERE short_name = $1", shortname)
	} else {
		_, err = dbmap.Select(&recominees, "SELECT * FROM recominee WHERE id <> $1 AND short_name = $2", currentBusinessId, shortname)
	}

	if err != nil && err == sql.ErrNoRows {
		log.Println("Warning: No rows found.")
		return false
	} else if err != nil {
		log.Println("Error: Can not retrieve recominee.")
		return true
	} else if len(recominees) > 0 {
		return true
	}

	return false
}

func fetchRecomineeDetailFromId(id interface{}) (aRecominee Recominee) {
	err := dbmap.SelectOne(&aRecominee, "SELECT * FROM recominee WHERE id = $1", id)
	// TODO : handle error properly
	if err != nil {
		log.Println(err.Error())
	}

	return
}

func fetchRecomineeDetailFromMasterId(masterId interface{}) (aRecominee Recominee) {
	err := dbmap.SelectOne(&aRecominee, "SELECT * FROM recominee WHERE master = $1", masterId)
	// TODO : handle error properly
	if err != nil {
		log.Println("Error fetching recominee detail, ", err.Error())
	}

	return
}
