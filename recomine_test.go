package main

import "testing"

func TestlenPlus(t *testing.T) {
	sum := lenPlus(2, 3)
	if sum != 4 {
		t.Error("Expected 4 got", sum)
	}
}
